/**
 * Array de itens de menu com a estrutura usada na montagem do componente do menu lateral (Treview)
 * @see src/app/@core/layout/components/side-navigation-menu
 * @see https://js.devexpress.com/Documentation/20_2/Guide/Themes_and_Styles/Icons/#Built-In_Icon_Library
 */
export const navigation = [
  {
    text : 'Terceirizados',
    path : '/terceirizado',
    icon : 'group',
    data : {
      permissions : {
        only : 'ADMIN'
      }
    }
  },
  {
    text : 'Meus Dados',
    path : '/terceirizado/meus-dados',
    icon : 'user',
    data : {
      permissions : {
        only : 'TERCEIRIZADO'
      }
    }
  },
  {
    text : 'Afastamentos',
    path : '/afastamento',
    icon : 'clock',
    data : {
      permissions : {
        only : 'afastamento:resource:view'
      }
    }
  },
  {
    text : 'Contratos',
    path : '/contrato',
    icon : 'folder',
    data : {
      permissions : {
        only : 'contrato:resource:view'
      }
    }
  },
  {
    text : 'Relatórios',
    path : '/relatorios',
    icon : 'file',
    data : {
      permissions : {
        only : 'report:resource:view'
      }
    }
  },

  {
    text : 'Configurações',
    icon : 'preferences',
    items : [
      {
        text : 'Deficiência',
        path : '/deficiencia',
        icon : 'user',
        data : {
          permissions : {
            only : 'tipo-afastamento:resource:view'
          }
        }
      },
      {
        text : 'Tipos de Afastamento',
        path : '/tipo-afastamento',
        icon : 'airplane',
        data : {
          permissions : {
            only : 'tipo-afastamento:resource:view'
          }
        }
      },
      {
        text : 'Cargos',
        path : '/cargo',
        icon : 'user',
        data : {
          permissions : {
            only : 'cargo:resource:view'
          }
        }
      },

      {
        text : 'Grau de Instrução',
        path : '/grau-instrucao',
        icon : 'hierarchy',
        data : {
          permissions : {
            only : 'grau-instrucao:resource:view'
          }
        }
      },
      {
        text : 'Raça',
        path : '/raca',
        icon : 'card',
        data : {
          permissions : {
            only : 'raca:resource:view'
          }
        }
      },
      {
        text : 'Unidades',
        path : '/unidade',
        icon : 'product',
        data : {
          permissions : {
            only : 'unidade:resource:view'
          }
        }
      },
      {
        text : 'Identidade de Gênero',
        path : '/identidade-de-genero',
        icon : 'user',
        data : {
          permissions : {
            only : 'identidade-de-genero:resource:view'
          }
        }
      }
    ]
  }
];
