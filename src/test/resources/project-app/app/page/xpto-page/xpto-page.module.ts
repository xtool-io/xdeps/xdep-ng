import {NgModule} from '@angular/core';
import {SharedModule} from '../../@shared/shared.module';
import {XptoPageComponent} from "./xpto-page.component";
import {XptoPageRoutingModule} from "./xpto-page-routing.module";


@NgModule({
  imports: [
    XptoPageRoutingModule,
    SharedModule
  ],
  declarations: [XptoPageComponent]
})
export class XptoPageModule {
}
