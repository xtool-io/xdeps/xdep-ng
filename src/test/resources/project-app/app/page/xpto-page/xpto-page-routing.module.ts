import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {XptoPageComponent} from "./xpto-page.component";

export const routes: Routes = [
  {path: '', component: XptoPageComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class XptoPageRoutingModule {
}
