import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NgxPermissionsGuard} from "ngx-permissions";
import {TerceirizadoPageModule} from './page/terceirizado-page/terceirizado-page.module';

const routes: Routes = [
  // {
  //   path : 'home',
  //   loadChildren : () => import('./page/home-page/home-page.module').then(m => m.XptoPageModule)
  // },
  {
    path : 'afastamento',
    loadChildren : () => import('./page/afastamento-page/afastamento-page.module').then(m => m.AfastamentoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'afastamento:resource:view'
      }
    }
  },
  {
    path : 'relatorios',
    loadChildren : () => import('./@reports/report-page.module').then(m => m.ReportPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'report:resource:view'
      }
    }
  },
  {
    path : 'tipo-afastamento',
    loadChildren : () => import('./page/@settings/tipo-afastamento-page/tipo-afastamento-page.module').then(m => m.TipoAfastamentoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'tipo-afastamento:resource:view'
      }
    }
  },
  {
    path : 'cargo',
    loadChildren : () => import('./page/@settings/cargo-page/cargo-page.module').then(m => m.CargoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'cargo:resource:view'
      }
    }
  },
  {
    path : 'contrato',
    loadChildren : () => import('./page/@settings/contrato-page/contrato-page.module').then(m => m.ContratoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'contrato:resource:view'
      }
    }
  },
  {
    path : 'grau-instrucao',
    loadChildren : () => import('./page/@settings/grau-instrucao-page/grau-instrucao-page.module').then(m => m.GrauInstrucaoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'grau-instrucao:resource:view'
      }
    }
  },
  {
    path : 'deficiencia',
    loadChildren : () => import('./page/@settings/deficiencia-page/deficiencia-page.module').then(m => m.DeficienciaPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'deficiencia:resource:view'
      }
    }
  },

  {
    path : 'identidade-de-genero',
    loadChildren : () => import('./page/@settings/identidade-de-genero-page/identidade-de-genero-page.module').then(m => m.IdentidadeDeGeneroPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'identidade-de-genero:resource:view'
      }
    }
  },
  {
    path : 'sexo',
    loadChildren : () => import('./page/@settings/sexo-page/sexo-page.module').then(m => m.SexoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'sexo:resource:view'
      }
    }
  },
  {
    path : 'raca',
    loadChildren : () => import('./page/@settings/raca-page/raca-page.module').then(m => m.RacaPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'raca:resource:view'
      }
    }
  },
  {
    path : 'terceirizado',
    loadChildren : () => import('./page/terceirizado-page/terceirizado-page.module').then(m => m.TerceirizadoPageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : ['ADMIN', 'TERCEIRIZADO']
      }
    }
  },
  {
    path : 'unidade',
    loadChildren : () => import('./page/@settings/unidade-page/unidade-page.module').then(m => m.UnidadePageModule),
    canActivate : [NgxPermissionsGuard],
    data : {
      permissions : {
        only : 'unidade:resource:view'
      }
    }
  },
  {
    path : '**',
    redirectTo : 'terceirizado'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes ,{ useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
