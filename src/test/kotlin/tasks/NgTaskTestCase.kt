package tasks

import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.info.BuildProperties
import org.springframework.mock.env.MockEnvironment
import xtool.xdepng.model.*
import xtool.xdepng.tasks.NgTask
import xtool.xdepng.tasks.NodeTask
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class NgTaskTestCase {

    private lateinit var env: MockEnvironment

    private lateinit var nodeTask: NodeTask

    private lateinit var ngTask: NgTask

    @BeforeEach
    fun init() {
        env = MockEnvironment()
            .withProperty("node.version", "12.13.0")
            .withProperty("ts-script.path", System.getProperty("user.dir").plus("/src/main/resources/ts-scripts"))
        val properties = Properties()
        properties.putAll(
            mapOf(
                "artifact" to "xdep-ng",
                "version" to "0.0.1"
            )
        )
        val buildProperties = BuildProperties(properties)
        println("env: $env")
        nodeTask = NodeTask(env)
        ngTask = NgTask(env, nodeTask, buildProperties)

        nodeTask.npm(
            args = listOf("i"),
            destination = Paths.get(env.getRequiredProperty("ts-script.path")),
            inheritIO = true
        )
        nodeTask.npx(
            args = listOf("tsc"),
            destination = Paths.get(env.getRequiredProperty("ts-script.path")),
            inheritIO = false
        )
        FileUtils.deleteDirectory(Paths.get("target/project-app").toFile())
        Files.createDirectories(Paths.get("target/project-app"))
        FileUtils.copyDirectory(File("src/test/resources/project-app"), File("target/project-app"))
    }

    @Test
    fun nodeVersion() {
//        val nodeTask = NodeTask(env)
        println("user.dir: ${Paths.get(System.getProperty("user.dir"))}")
        nodeTask.node(listOf("-v"))
    }

    @Test
    fun addNewRoute() {
        val ngRouteModule = NgRoutingModule(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/app-routing.module.ts"))
        val ngModule = NgModule(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/page/xpto-page/xpto-page.module.ts"))

        ngTask.addRoute(
            ngRouteModule = ngRouteModule,
            newNgRoute = NgRoute(
                path = "ponto",
                loadChildren = NgRoute.loadChildenFrom(ngRouteModule, ngModule),
                canActivate = listOf("NgxPermissionsGuard"),
                data = mapOf(
                    "permissions" to mapOf(
                        "only" to "unidade:resource:view2"
                    )
                )
            ),
            destination = Paths.get(env.getRequiredProperty("ts-script.path"))
        )

    }

    @Test
    fun addNewNavigation() {
        val ngNavigation = NgNavigation(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/app-navigation.ts"))
        ngTask.addNavigation(
            ngNavigation, NgNavigationItem(
                path = "/pontoabc",
                text = "Ponto ABC",
                data = mapOf(
                    "permissions" to mapOf(
                        "data" to "ponto:resource:view"
                    )
                )
            ),
            destination = Paths.get(env.getRequiredProperty("ts-script.path"))
        )
    }

    @Test
    fun addNewNavigationWithParent() {
        val ngNavigation = NgNavigation(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/app-navigation.ts"))
        ngTask.addNavigation(
            ngNavigation, NgNavigationItem(
                path = "/pontoabc",
                text = "Ponto ABC",
                data = mapOf(
                    "permissions" to mapOf(
                        "data" to "ponto:resource:view"
                    )
                )
            ),
            destination = Paths.get(env.getRequiredProperty("ts-script.path")),
            parentName = "Configurações"
        )
    }

    @Test
    fun registerComponent() {
        val ngModule = NgModule(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/page/xpto-page/xpto-page.module.ts"))
        val ngComponentTs = NgComponentTs(Paths.get("${System.getProperty("user.dir")}/target/project-app/app/page/xpto-page/xpto-page.component.ts"))
        ngTask.registerComponent(
            ngModule,
            ngComponentTs,
            destination = Paths.get(env.getRequiredProperty("ts-script.path")))
    }

}
