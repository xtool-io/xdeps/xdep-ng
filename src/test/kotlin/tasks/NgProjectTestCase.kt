package tasks

import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import xtool.xdepng.model.NgProject
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class NgProjectTestCase {

    @BeforeEach
    fun init() {
        FileUtils.deleteDirectory(Paths.get("target/project-app").toFile())
        Files.createDirectories(Paths.get("target/project-app"))
        FileUtils.copyDirectory(File("src/test/resources/project-app"), File("target/project-app"))
    }

//    @Test
    fun ngProjectTest() {
        val ngProject = NgProject(Paths.get("/home/jcruz/git/sds/ecomdac/src/main/webapp"))
//        println(ngProject.htmlComponents.asSequence().toList())
//        println(ngProject.tsComponents.asSequence().toList())
//        println(ngProject.modules.asSequence().toList())
//        println(ngProject.routingModules.asSequence().toList())
        println("*** HtmlComponents")
        ngProject.htmlComponents.forEach {
            println(it)
        }
        println("*** TsComponents")
        ngProject.tsComponents.forEach {
            println(it)
        }
        println("*** Modules")
        ngProject.modules.forEach {
            println(it)
        }
        println("*** PageModules")
        ngProject.pageModules.forEach {
            println(it)
        }
        println("*** RoutingModules")
        ngProject.routingModules.forEach {
            println(it)
        }
        println("*** PageRoutingModules")
        ngProject.pageRoutingModules.forEach {
            println(it)
        }
        println("*** TsListComponent")
        ngProject.tsListComponents.forEach {
            println(it)
        }
        println("*** HtmlListComponent")
        ngProject.htmlListComponents.forEach {
            println(it)
        }
        println("*** TsDetailComponent")
        ngProject.tsDetailComponents.forEach {
            println(it)
        }
        println("*** HtmlDetailComponent")
        ngProject.htmlDetailComponents.forEach {
            println(it)
        }
    }
}
