/**
 * Script que adiciona uma rota ao arquivo -routing.module.ts.
 * Caso o path já existe a rota não será adicionada.
 */
import {Project, SyntaxKind} from "ts-morph";
import {includes} from "lodash";

//Argumento com o caminho completo do arquivo de rota.
const routingModulePath = process.argv[2];
//Json com a nova rota
const newJsonRoute = (process.argv[3]).replace(/"/g, "'")
//Nome do path da nova rota
const newRoutePath = process.argv[4]

const project = new Project();
const routingModuleSourceFile = project.addSourceFileAtPath(routingModulePath);

const routesArray = routingModuleSourceFile.getVariableDeclaration('routes')?.getInitializerIfKindOrThrow(SyntaxKind.ArrayLiteralExpression);
const routesArrayElements = routesArray?.getElements();

const paths = routesArray
    ?.getDescendantsOfKind(SyntaxKind.PropertyAssignment)
    ?.filter(prop => prop.getName() === "path")
    ?.map(prop => prop.getInitializerIfKindOrThrow(SyntaxKind.StringLiteral).getLiteralValue())


const hasRoutePath = includes(paths, newRoutePath)

if (hasRoutePath) process.exit(1)

routesArray?.insertElement(routesArrayElements?.length - 1, newJsonRoute);
routingModuleSourceFile.saveSync();
