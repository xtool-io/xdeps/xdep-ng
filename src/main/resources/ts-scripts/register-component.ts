/**
 * Script que adiciona um componente a um módulo Angular.
 * Param 1: Caminho absoluto do módulo.
 * Param 2: Caminho absoluto do componente.
 */
import {Project, SyntaxKind} from "ts-morph";
import {includes} from "lodash";

const _ = require('lodash');
const path = require('path');
const args = require('yargs').argv;

const sourcePath = process.argv[2];
const componentPath = process.argv[3]
const componentName = process.argv[4]

const project = new Project();
const sourceFile = project.addSourceFileAtPath(sourcePath);

const moduleClassName = _.upperFirst(_.camelCase(path.basename(sourcePath).replace(/.ts$/, '')))
const componentClassName = _.upperFirst(_.camelCase(path.basename(componentPath).replace(/.ts$/, '')))

const moduleClass = sourceFile.getClassOrThrow(moduleClassName)
const moduleDecorator = moduleClass.getDecoratorOrThrow('NgModule')
const declarationArray = moduleDecorator
    .getFirstDescendantByKindOrThrow(SyntaxKind.ObjectLiteralExpression)
    .getProperty('declarations')
    ?.getFirstDescendantByKindOrThrow(SyntaxKind.ArrayLiteralExpression)

const declarations = declarationArray
    ?.getDescendantsOfKind(SyntaxKind.Identifier)
    ?.map(ident => ident.getType().getSymbol()?.getName())


const hasDeclaration = includes(declarations, componentName)

if (hasDeclaration) process.exit(1)

sourceFile.addImportDeclaration({
    namedImports: componentClassName,
    moduleSpecifier: path.relative(sourcePath, componentPath)
        .replace(/.ts$/, '')
        .replace(/^\.\.\//, './')
})
declarationArray?.addElement(componentClassName)

sourceFile.saveSync();
