import {Project, SyntaxKind} from "ts-morph";
import {includes} from "lodash";

const sourcePath = process.argv[2];
const newNavigation = (process.argv[3]).replace(/"/g, "'")
const newNavigationPath = process.argv[4]
const parentNav = process.argv[5]

const project = new Project();
const sourceFile = project.addSourceFileAtPath(sourcePath);

let navigationArray;


if (parentNav) {
    navigationArray = sourceFile.getVariableDeclaration('navigation')
        ?.getInitializerIfKindOrThrow(SyntaxKind.ArrayLiteralExpression)
        ?.getChildrenOfKind(SyntaxKind.ObjectLiteralExpression)
        ?.find(objExp => objExp.getPropertyOrThrow('text')
            .getFirstChildByKindOrThrow(SyntaxKind.StringLiteral)
            .getText() === `'${parentNav}'`)
        ?.getPropertyOrThrow('items')
        ?.getFirstChildByKindOrThrow(SyntaxKind.ArrayLiteralExpression)

} else {
    navigationArray = sourceFile.getVariableDeclaration('navigation')?.getInitializerIfKindOrThrow(SyntaxKind.ArrayLiteralExpression)
}

const navigationArrayElements = navigationArray?.getElements();
const paths = navigationArray
    ?.getDescendantsOfKind(SyntaxKind.PropertyAssignment)
    ?.filter(prop => prop.getName() === "path")
    ?.map(prop => prop.getInitializerIfKindOrThrow(SyntaxKind.StringLiteral).getLiteralValue())


const hasNavigationPath = includes(paths, newNavigationPath)

if (hasNavigationPath) process.exit(1)

let insertNavItemIdx = parentNav ? 0 : navigationArrayElements?.length - 1;

navigationArray?.insertElement(insertNavItemIdx, newNavigation);
sourceFile.saveSync();
