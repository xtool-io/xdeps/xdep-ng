package xtool.xdepng

import org.slf4j.LoggerFactory
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import javax.annotation.PostConstruct

@Configuration
@ComponentScan
class XdepNg(
    private val env: Environment,
    private val buildProperties: BuildProperties
) {

    private val log = LoggerFactory.getLogger(XdepNg::class.java)

    @PostConstruct
    fun init() {
        log.debug("xdep-ng carregado com sucesso.")
    }
}
