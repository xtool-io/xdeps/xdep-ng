package xtool.xdepng.tasks

import org.fusesource.jansi.Ansi
import org.slf4j.LoggerFactory
import org.springframework.boot.info.BuildProperties
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xtool.xdepng.model.*
import xtool.xdepng.utils.JsonHelper
import java.nio.file.Path
import java.nio.file.Paths


/**
 * Classe com serviços para manipulação de arquivos de projeto Angular.
 */
@Service
class NgTask(
    private val env: Environment,
    private val nodeTask: NodeTask,
    private val buildProperties: BuildProperties,

    ) {

    private val log = LoggerFactory.getLogger(NgTask::class.java)

    private val workspacePath = Paths.get(System.getProperty("user.dir"))

    private val defaultDestination = Paths.get(env.getRequiredProperty("ts-script.path")).resolve(buildProperties.artifact).resolve(buildProperties.version)

    /**
     * Retorna o projeto Angular para o caminho especificado.
     */
    fun getProject(path: Path = Paths.get(System.getProperty("user.dir")).resolve("src/main/webapp")): NgProject {
        return NgProject(path)
    }

    /**
     * Adicionar um item de navegação ao arquivo app.navigation.ts
     */
    fun addNavigation(
        ngNavigation: NgNavigation,
        newNgNavigationItem: NgNavigationItem,
        parentName: String? = null,
        destination: Path = defaultDestination
    ) {
        val pNgNavItem = JsonHelper.serialize(newNgNavigationItem)
        val pNgNavigationModule = ngNavigation.path.toString()
        log.debug("addNavigation(ngNavigation={}, newNgNavigationItem={}, parentName={}, destination={})", ngNavigation, newNgNavigationItem, parentName, destination)
        log.debug("  > pNgNavItem={} ", pNgNavItem)
        log.debug("  > pNgNavigationModule={}", pNgNavigationModule)
        val ret = nodeTask.node(
            args = listOf("build/add-navigation.js", pNgNavigationModule, "'${pNgNavItem}'", newNgNavigationItem.path.toString(), parentName),
            destination = destination
        )
        when (ret) {
            0 -> log.info("${Ansi.ansi().fgBrightBlue().a("(update)").reset()} ${workspacePath.relativize(Paths.get(pNgNavigationModule))}")
            else -> log.info("${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", workspacePath.relativize(Paths.get(pNgNavigationModule)))
        }
    }

    fun addRoute(
        ngRouteModule: NgRoutingModule,
        newNgRoute: NgRoute,
        destination: Path = defaultDestination
    ) {
        val newJsonRoute = JsonHelper.serialize(newNgRoute)
        log.debug("addRoute(ngRouteModule={}, newNgRoute={}, destination={})", ngRouteModule, newJsonRoute, destination)
        log.debug("  > newJsonRoute={}", newJsonRoute)
        val ret = nodeTask.node(
            args = listOf("build/add-route.js", ngRouteModule.path.toString(), "'${newJsonRoute}'", newNgRoute.path),
            destination = destination
        )
        when (ret) {
            0 -> log.info("${Ansi.ansi().fgBrightBlue().a("(update)").reset()} ${workspacePath.relativize(ngRouteModule.path)}")
            else -> log.info("${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", workspacePath.relativize(ngRouteModule.path))
        }
    }

    fun registerComponent(
        ngModule: NgModule,
        ngComponentTs: NgComponentTs,
        destination: Path = defaultDestination
    ) {
        log.debug("registerComponent(ngModule={}, ngComponentTs={}, destination={})", ngModule, ngComponentTs, destination)
        val ret = nodeTask.node(
            args = listOf("build/register-component.js", ngModule.path.toString(), ngComponentTs.path.toString(), ngComponentTs.className),
            destination = destination,
        )
        when (ret) {
            0 -> log.info("${Ansi.ansi().fgBrightBlue().a("(update)").reset()} ${workspacePath.relativize(ngModule.path)}")
            else -> log.info("${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", workspacePath.relativize(ngModule.path))
        }
    }

    fun format(
        ngFile: NgFile,
        destination: Path = defaultDestination
    ) {
        nodeTask.npx(
            args = listOf("prettier", ngFile.path.toString(), "--write", "--tab-width 2", "--print-width 170", "--single-quote"),
            destination = destination,
            inheritIO = false
        )
    }

}
