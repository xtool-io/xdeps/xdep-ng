package xtool.xdepng.tasks

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Task que contém operação para Node.
 */
@Component
class NodeTask(private val env: Environment) {

    fun node(args: List<String?>, destination: Path = Paths.get(System.getProperty("user.dir")), inheritIO: Boolean = true): Int {
        return run("node", args, destination, inheritIO)
    }

    fun npx(args: List<String>, destination: Path = Paths.get(System.getProperty("user.dir")), inheritIO: Boolean = true): Int {
        return run("npx", args, destination, inheritIO)
    }

    fun npm(args: List<String>, destination: Path = Paths.get(System.getProperty("user.dir")), inheritIO: Boolean = true): Int {
        return run("npm", args, destination, inheritIO)
    }

    private fun run(command: String, args: List<String?>, destination: Path, inheritIO: Boolean = true): Int {
        val nodeVersion = env.getRequiredProperty("node.version")
        val nodeHome = "${System.getProperty("user.home")}/.nvm/versions/node/v${nodeVersion}/bin/"
        val processBuilder = ProcessBuilder()
        val env = processBuilder.environment()
        var path = env["PATH"]
        path = path + File.pathSeparator + nodeHome
        env["PATH"] = path
        processBuilder.directory(destination.toFile())
        processBuilder.command("/bin/bash", "-c", "$command ${args.joinToString(" ")}")
        if (inheritIO) processBuilder.inheritIO()
        val process = processBuilder.start()
        return process.waitFor()
    }
}
