package xtool.xdepng.command.provider

import org.springframework.stereotype.Component
import xtool.xdepng.tasks.NgTask

@Component
class NgPageModuleValueProvider(private val ngTask: NgTask) : Iterable<String> {
    override fun iterator(): Iterator<String> {
        val pageModules = ngTask.getProject().pageModules
        return pageModules.asSequence()
            .map { it.className }
            .iterator()
    }
}
