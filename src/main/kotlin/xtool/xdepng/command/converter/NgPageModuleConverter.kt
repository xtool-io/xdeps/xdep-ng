package xtool.xdepng.command.converter

import org.springframework.stereotype.Component
import picocli.CommandLine
import xtool.xdepng.model.NgPageModule
import xtool.xdepng.tasks.NgTask

@Component
class NgPageModuleConverter(private val ngTask: NgTask) : CommandLine.ITypeConverter<NgPageModule> {
    override fun convert(value: String?): NgPageModule {
        val pageModules = ngTask.getProject().pageModules
        return pageModules.asSequence().find { it.className == value }!!
    }
}
