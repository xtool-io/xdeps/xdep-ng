package xtool.xdepng.utils

import org.fusesource.jansi.Ansi
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

object FileHelper {

    private val log = LoggerFactory.getLogger(FileHelper::class.java)

    fun updateFile(path: Path, content: ByteArray) {
        if (!Files.readAllBytes(path).contentEquals(content)) {
            val os = Files.newOutputStream(path)
            os.write(content)
            os.flush()
            os.close()
            log.info("${Ansi.ansi().fgBrightGreen().a("(update)").reset()} $path")
            return
        }
        log.info("${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", path)
    }

}
