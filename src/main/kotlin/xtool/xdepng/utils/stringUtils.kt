package xtool.xdepng.utils


fun String.indexOfPattern(regex: Regex): IntRange {
    return regex.find(this)?.let { it.range } ?: IntRange(-1, -1)
}

fun String.indexOfFirstArray(): IntRange {
    var i = 0
    var startArrayIdx = -1
    var endArrayIdx = -1
    var qtdArrays = 0
    do {
        val c: Char = this[i]
        if (c == '[') {
            if (qtdArrays == 0) {
                startArrayIdx = i
            }
            qtdArrays++
        } else if (c == ']') {
            if (qtdArrays == 1) {
                endArrayIdx = i + 1
            }
            qtdArrays--
        }
        i++
    } while (qtdArrays > 0)
    return IntRange(startArrayIdx, endArrayIdx)
}

