package xtool.xdepng.utils

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.PrettyPrinter
import com.fasterxml.jackson.core.json.JsonWriteFeature
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature


object JsonHelper {
    private val mapper = ObjectMapper()

    init {
        mapper.disable(JsonWriteFeature.QUOTE_FIELD_NAMES.mappedFeature());
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(JsonParser.Feature.IGNORE_UNDEFINED, true);
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        mapper.configure(JsonParser.Feature.ALLOW_TRAILING_COMMA, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }

    fun <T> deserialize(content: String?, clazz: TypeReference<T>?): T {
        return mapper.readValue(content, clazz)
    }

    fun <T> serialize(obj: T, pp: PrettyPrinter? = DefaultPrettyPrinter()): String {
        return mapper.writeValueAsString(obj)
    }
}
