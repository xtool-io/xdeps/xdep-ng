package xtool.xdepng.config

import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.core.env.Environment
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import xtool.xdepng.tasks.NodeTask
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

@Configuration
class AppReadyConfig(
    private val buildProperties: BuildProperties,
    private val nodeTask: NodeTask,
    private val env: Environment,
) {

    private val log = LoggerFactory.getLogger(AppReadyConfig::class.java)

    /**
     * Realiza a instalação dos ts-scripts
     */
    @EventListener
    fun onApplicationEvent(event: ContextRefreshedEvent) {
        val destination = Paths.get(env.getRequiredProperty("ts-script.path")).resolve(buildProperties.artifact).resolve(buildProperties.version)
        if (Files.exists(destination)) return
        FileUtils.createParentDirectories(destination.toFile())
        val resolver = PathMatchingResourcePatternResolver()
        val resources = resolver.getResources("classpath:/ts-scripts/*")
        for (resource in resources) {
            if (resource.exists() && resource.isReadable && resource.contentLength() > 0) {
                val url = resource.url
                log.debug("Copiando arquivo: {}, ", url)
                FileUtils.copyURLToFile(url, File(destination.toFile().absolutePath.plus("/").plus(resource.filename)))
            }
        }
        nodeTask.npm(
            args = listOf("i"),
            destination = destination,
            inheritIO = false
        )
        nodeTask.npx(
            args = listOf("tsc"),
            destination = destination,
            inheritIO = false
        )
        log.info("ts-scripts instalados com sucesso em: $destination")
    }


}
