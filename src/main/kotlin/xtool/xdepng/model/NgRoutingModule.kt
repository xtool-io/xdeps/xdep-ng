package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um arquivo routing.module.ts
 */
open class NgRoutingModule(path: Path) : NgFile(path) {


    open val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix("-routing.module.ts")
                .plus("RoutingModule")
        )


    override fun toString(): String {
        return "NgRoutingModule(className=$className)"
    }

}
