package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um módulo Angular
 */
class NgPageModule(path: Path) : NgModule(path) {


    override val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix("-page.module.ts")
                .plus("PageModule")
        )


    override fun toString(): String {
        return """NgPageModule(className=$className)"""
    }

}
