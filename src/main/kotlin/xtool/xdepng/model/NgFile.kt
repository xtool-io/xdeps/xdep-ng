package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.notExists

open class NgFile(val path: Path) {

    init {
        if (path.notExists()) throw FileNotFoundException("Arquivo não encontrado: $path")
    }

    val fileName: String
        get() = FilenameUtils.getBaseName(this.path.toString())

    val content: ByteArray
        get() = Files.readAllBytes(path)


    val contentAsString: String
        get() = String(this.content)
}
