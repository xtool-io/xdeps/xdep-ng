package xtool.xdepng.model

import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.name

/**
 * Classe que representa um projeto Angular.
 */
data class NgProject(private val path: Path) {

    val modules: Iterator<NgModule> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith(".module.ts") }
            .filter { path -> !path.name.endsWith("-routing.module.ts") }
            .map { NgModule(it) }
            .iterator()
    }

    val pageModules: Iterator<NgPageModule> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-page.module.ts") }
            .filter { path -> !path.name.endsWith("-page-routing.module.ts") }
            .map { NgPageModule(it) }
            .iterator()
    }

    val routingModules: Iterator<NgRoutingModule> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-routing.module.ts") }
            .map { NgRoutingModule(it) }
            .iterator()
    }

    val pageRoutingModules: Iterator<NgPageRoutingModule> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-page-routing.module.ts") }
            .map { NgPageRoutingModule(it) }
            .iterator()
    }

    val htmlComponents: Iterator<NgComponentHtml> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith(".component.html") }
            .map { NgComponentHtml(it) }
            .iterator()
    }

    val tsComponents: Iterator<NgComponentTs> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith(".component.ts") }
            .map { NgComponentTs(it) }
            .iterator()
    }

    val htmlListComponents: Iterator<NgListComponentHtml> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-list.component.html") }
            .map { NgListComponentHtml(it) }
            .iterator()
    }

    val htmlDetailComponents: Iterator<NgDetailComponentHtml> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-detail.component.html") }
            .map { NgDetailComponentHtml(it) }
            .iterator()
    }

    val tsListComponents: Iterator<NgListComponentTs> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-list.component.ts") }
            .map { NgListComponentTs(it) }
            .iterator()
    }

    val tsDetailComponents: Iterator<NgDetailComponentTs> by lazy {
        Files.walk(path.resolve("app"))
            .filter { path -> !path.toString().contains("@core") }
            .filter { path -> !path.toString().contains("@shared") }
            .filter { path -> !path.toString().contains("@security") }
            .filter { path -> path.name.endsWith("-detail.component.ts") }
            .map { NgDetailComponentTs(it) }
            .iterator()
    }
}
