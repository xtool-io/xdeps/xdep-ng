package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um módulo Angular
 */
open class NgModule(path: Path) : NgFile(path) {

    open val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix(".module.ts")
                .plus("Module")
        )


    override fun toString(): String {
        return """NgModule(className=$className)"""
    }

}
