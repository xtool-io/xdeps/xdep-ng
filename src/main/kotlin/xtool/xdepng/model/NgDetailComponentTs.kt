package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um módulo Angular
 */
class NgDetailComponentTs(path: Path) : NgFile(path) {


    val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix("-detail.component.ts")
                .plus("DetailComponent")
        )

    override fun toString(): String {
        return """NgDetailComponentTs(className=$className)"""
    }

}
