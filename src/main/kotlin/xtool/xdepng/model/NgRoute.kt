package xtool.xdepng.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRawValue


/**
 * Classe que representa uma Route do Angular
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
data class NgRoute(
    @JsonProperty("path") val path: String,
    @JsonProperty("loadChildren") @JsonRawValue val loadChildren: String? = null,
    @JsonProperty("pathMatch") val pathMatch: String? = null,
    @JsonProperty("redirectTo") val redirectTo: String? = null,
    @JsonProperty("component") @JsonInclude(JsonInclude.Include.NON_EMPTY) @JsonRawValue val component: String? = null,
    @JsonProperty("canActivate") @JsonRawValue val canActivate: List<String>? = null,
    @JsonProperty("children") @JsonRawValue val children: List<NgRoute>? = null,
    @JsonProperty("resolve") val resolve: Map<String, String>? = null,
    @JsonProperty("data") var data: Map<String, Map<String, String>> = mapOf(),
) {

    companion object {
        fun loadChildenFrom(ngRoutingModule: NgRoutingModule, ngModule: NgModule): String {
            return """() => import("${ngRoutingModule.path.relativize(ngModule.path).toString().replace("../", "./").removeSuffix(".ts")}").then(m => m.${ngModule.className})"""
        }
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NgRoute

        return path == other.path
    }

    override fun hashCode(): Int {
        return path.hashCode()
    }


}

