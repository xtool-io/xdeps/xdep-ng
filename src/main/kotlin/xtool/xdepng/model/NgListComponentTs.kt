package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um módulo Angular
 */
class NgListComponentTs(path: Path) : NgFile(path) {


    val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix("-list.component.ts")
                .plus("ListComponent")
        )

    override fun toString(): String {
        return """NgListComponentTs(className=$className)"""
    }

}
