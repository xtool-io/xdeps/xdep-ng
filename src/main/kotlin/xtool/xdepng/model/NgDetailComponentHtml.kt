package xtool.xdepng.model

import java.nio.file.Path
import kotlin.io.path.name

/**
 * Classe que representa um módulo Angular
 */
class NgDetailComponentHtml(path: Path) : NgFile(path) {

    override fun toString(): String {
        return """NgDetailComponentHtml(name=${path})"""
    }

}
