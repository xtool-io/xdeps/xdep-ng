package xtool.xdepng.model

import org.apache.commons.io.FilenameUtils
import strman.Strman
import java.nio.file.Path

/**
 * Classe que representa um arquivo routing.module.ts
 */
class NgPageRoutingModule(path: Path) : NgRoutingModule(path) {


    override val className: String
        get() = Strman.toStudlyCase(
            FilenameUtils.getName(this.path.toString())
                .removeSuffix("-page-routing.module.ts")
                .plus("PageRoutingModule")
        )


    override fun toString(): String {
        return "NgPageRoutingModule(className=$className)"
    }

}
