package xtool.xdepng.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class NgNavigationItem(
    @JsonProperty("text") val text: String,
    @JsonProperty("path") var path: String? = null,
    @JsonProperty("icon") var icon: String? = null,
    @JsonProperty("items") var items: MutableList<NgNavigationItem>? = null,
    @JsonProperty("data") var data: Map<String, Map<String, String>>? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NgNavigationItem

        return text == other.text
    }

    override fun hashCode(): Int {
        return text.hashCode()
    }
}
